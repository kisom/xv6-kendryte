I'm trying to port xv6 to the MAix BiT.

## Building:

Assuming the toolchain has been installed to `/opt/kendryte-toolchain`
(see the links below for more info), then

```
mkdir build && cd build && \
cmake -DPROJ=xv6 -DTOOLCHAIN=/opt/kendryte-toolchain/bin .. && make

## Links

+ [Original post](https://ai6ua.net/blog/2019/12/03/hello-maix-bit/)
+ [Toolchain setup notes](https://ai6ua.net/notes/kendryte.html)
